## Getting Started

Run at the development server:

```bash
npm run dev
# or
yarn dev
```

Run at the production server:

```bash
npm run build
npm run start
# or
yarn build
yarn start
```
